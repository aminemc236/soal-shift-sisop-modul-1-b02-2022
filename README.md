# Soal-shift-sisop-modul-1-B02-2022
# __Soal 1__

## register.sh
## main.sh

<hr>

# __Soal 2__

## soal2_forensic_dapos.sh

1. Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

    a. Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

    ```bash
    if [ ! -d "$folder" ]
    then
        mkdir $folder
    else
        rm -r $folder
        mkdir $folder
        echo "forensic_log_website_daffainfo_log was created"
    fi
    ```
    > Membuat folder forensic_log_website_daffainfo_log menggunakan mkdir dengan bantuan kondisi if else, apabila folder belum tersedia maka folder akan dibuat. Apabila folder sudah tersedia maka folder aka dihapus terlebih dahulu (rm) dan akan dibuatkan folder yang baru

    b. Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

    ```bash
    cat $log | 
    awk ' END {
        print "Rata-rata serangan adalah sebanyak ", (NR-1)/12, "requests per jam"}
    ' >> $folder/ratarata.txt
    ```
    > cat $log untuk menampilkan isi dari file log_website_daffainfo.log dengan menggunakan awk maka isi file akan diperiksa per barisnya. NR adalah jumlah total baris dalam file dikurangi satu karena baris pertama adalah judul. Kemudian memasukkan jumlah rata-ratanya ke dalam file bernama ratarata.txt ke dalam folder forensic_log_website_daffainfo_log.

    c. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

    ```bash
    cat $log |
    awk -F: '{$1
        arr[$1]++}
        END {
            temp
            max=0
            for (i in arr){
                if(max < arr[i]){
                    temp = i
                    max = arr[temp]
                }
            }
            print "IP yang paling banyak mengakses server adalah: " temp " sebanyak " max " requests\n"}
    ' >> $folder/result.txt
    ```
    > menggunakan awk -F<value> sebagai pemisah, dimana value nya merupakan ‘:’. Selanjutnya menyimpan argumen pertama dalam array (arr[$1]++). Menggunakan looping for dan conditional statements if untuk menentukan nilai terbesarnya. Lalu hasil akan dimasukkan kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

    d. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

    ```bash
    cat $log |
    awk '/curl/ {++n}
        END {
        print "Ada " n " request yang menggunakan curl sebagai user-agent\n"}
    ' >> $folder/result.txt
    ```
    > Menggunakan /curl/ (Client URL) untuk mengecek url yang digunakan user-agent dan setiap pemeriksaannya jika benar setiap index nya disimpan dalam variabel ‘n’. Lalu hasil akan dimasukkan  kedalam file result.txt kedalam folder yang sudah dibuat sebelumnya.

    e. Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

    ```bash
    cat $log |
    awk -F: '/2022:02/ {print $1 " Jam 2 pagi"}
    ' | uniq >>$folder/result.txt 
    ```

    > /2022:02/  merupakan selection criteria dan {print $1..} untuk menampilkan argumen pertama yang mengandung selection criteria tersebut. Perintah uniq untuk menghapus duplikasi baris. Lalu hasil akan dimasukkan  kedalam file result.txt kedalam folder yang sudah dibuat sebelumnya.

<hr>

# __Soal 3__
1. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untukmemperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer. Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/

    a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
    
    ```
    minute_logger(){
        #name for .log files
        TIMESTAMP="$(date +'%Y%m%d%H%M%S')"
        format="metrics_$TIMESTAMP"

        #go to where .log files is stored
        cd ~/log

        #write metrics to .log
        free -m >> $format.log
        du -sh ~ >> $format.log

        exit
    }
    ```
    > fungsi minute_logger digunakan untuk membuat file .log dan mencatat metrics yang diminta


    b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
    ```
    init_cron(){
        #path
        LOGPATH="/home/$USER/log"
        SCRIPTPATH="/home/$USER/log/script"

        #create directory if needed
        mkdir -p $LOGPATH
        mkdir -p $SCRIPTPATH

        #copy script to respective directory
        cp ./minute_log.sh $SCRIPTPATH/minute_log.sh
        cp ./aggregate_minutes_to_hourly_log.sh $SCRIPTPATH/aggregate_minutes_to_hourly_log.sh

        #create cron to run both script
        echo "* * * * * $SCRIPTPATH/minute_log.sh minute_logger" > mycron
        echo "*/2 * * * * $SCRIPTPATH/aggregate_minutes_to_hourly_log.sh" >> mycron

        crontab mycron
        rm mycron
    }


    ${1:-init_cron}
    ```
    > fungsi init_cron() digunakan untuk menginisiasi cron untuk script minute_logger.sh dan aggregate_minutes_to_hourly_log.sh dengan cara menyimpan kedua script tersebut di tempat yang ditentukan (SCRIPTPATH) kemudian menuliskan command untuk memanggil kedua script tersebut ke cron.

    c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
    ```
    get_mean(){
        mean=$(printf "%s\n" $input | awk '{ total += $1 } END { print total/NR }')
        memMean+=$mean" "
    }


    get_max(){
        max=$(printf "%s\n" $input | awk 'BEGIN{a=0}{if ($1>0+a) a=0+$1 fi} END{print a}')
        memMax+=$max" "
    }


    get_min(){
        min=$(printf "%s\n" $input | awk 'BEGIN{a=2^32}{if ($1<0+a) a=0+$1 fi} END{print a}')
        memMin+=$min" "
    }



    hour_logger(){
        cd ~/log

        TIMESTAMP="$(date +'%Y%m%d%H')"
        format="metrics_agg_$TIMESTAMP" 
        touch $format.log

        declare -a memItem=("\$2" "\$3" "\$4" "\$5" "\$6" "\$7")
        declare -a swapItem=("\$2" "\$3" "\$4")

        for item2 in "Mem" "Swap"; do
                final_text=""
                memMean=""
                memMin=""
                memMax=""
                if [[ $item2 == "Mem" ]]
                then
                        for item in ${memItem[@]}; do
                                input=$(cat metrics_[0-9]*.log | grep $item2 | awk "{print $item}")
                                get_mean
                                get_max
                                get_min
                        done
                elif [[ $item2 == "Swap" ]]
                then
                        for item in ${swapItem[@]}; do
                                input=$(cat metrics_[0-9]*.log | grep $item2 | awk "{print $item}")
                                get_mean
                                get_max
                                get_min
                        done    
                fi
                final_text+=$item2"_mean : "$memMean$'\n'
                final_text+=$item2"_max : "$memMax$'\n'
                final_text+=$item2"_min : "$memMin$'\n'
                echo "$final_text" | column -t >> $format.log
        done
        rm metrics_[0-9]*.log
    }

    ${1:-hour_logger}
    ```
    > fungsi hour_logger membaca data .log dengan cara piping cat -> grep -> awk. cat digunakan untuk menampilkan isi dari semua .log, grep digunakan untuk mengambil semua baris yang diinginkan, awk digunakan untuk membaca kolom/input tertentu dari baris. Kolom yang dibaca kemudian dioper ke fungsi agregat untuk kemudian diproses. Fungsi agregat akan mengkonstruksi string output yang kemudian digabung menjadi final_text. final_text kemudian dipadding dengan column -t dan disimpan di file .log. Terakhir, file .log menit akan dihapus karena sudah tidak digunakan.

    d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
